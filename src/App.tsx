import React from 'react';
import Select from "./components/Select/Select";
import {API, requestOptions} from './utils/index';

function App() {
    let options: any[] = [{key: 1, value: 'C#'}, {key: 2, value: 'JavaScript'}, {key: 3, value: 'Python'}, { key: 4, value: 'PostgreSQL'}, { key: 5, value: 'PHP'}];

    const getData = (): any[] => {
        fetch(API.GET_OPTIONS, requestOptions('GET', {}))
            .then(response => response.json())
            .then(
                (data) => {
                    console.log('data', data);
                    //Предполагается, что список пунктов выпадающего списка будет лежать в массиве options
                    //который потом передается в компонент Select
                    options = data.options;
                }
            );
        return options;
    }

    return (
        <div>
            <Select isMultiple={true} options={options}>
            </Select>
        </div>
    );
}

export default App;
