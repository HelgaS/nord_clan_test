export const API = {
    GET_OPTIONS: "https://example.com/get",
    SEARCH_OPTIONS: "https://example.com/search"
}

export function transformToArray(data: any): any[]{
    return Array.from(data);
}

export function checkIndexOf(data: any[], item_check: any): boolean {
    return (data.indexOf(item_check) !== -1);
}

export function requestOptions(method: string, body: object): object {
    return {
        method: method,
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(body)
    }
}