import React, { Component, createRef} from 'react';
import './Select.scss';
import Chevron from '@assets/chevron.svg';
import Close from '@assets/close.svg';
import {API, checkIndexOf, transformToArray, requestOptions} from "../../utils/index";

/**
 * Описание типов, которые можно принимать от родительского компонента
 * */
interface Props {
    isMultiple: boolean;
    options: any[];
}

/**
 * Описание типов, которые используются в состоянии компонента
 * */
interface ISelectState {
    isAppear: boolean;
    result: string;
    isMultiple: boolean;
    selected_items: string[];
    itemIsSearch: boolean;
    isLoading: boolean;
}

/**
 * Компонент Select
 * @extends {Component<Props>}
 */
export class Select extends Component<Props, ISelectState> {

    private readonly selectInput: React.RefObject<HTMLInputElement>;
    private readonly selectSearchDiv: React.RefObject<HTMLDivElement>;
    private readonly selectPopupMenu: React.RefObject<HTMLDivElement>;
    private readonly selectIconChevron: React.RefObject<HTMLDivElement>;

    /**
     * Конструктор
     * @constructor
     * @param {Props} props - Атрибуты, которые были получены от родительского компонента
     */
    constructor(props: Props) {
        super(props);

        this.state = {
            isAppear: false,
            result: '',
            isMultiple: this.props.isMultiple,
            selected_items: [],
            itemIsSearch: true,
            isLoading: false,
        };

        this.selectInput = createRef();
        this.selectPopupMenu = createRef();
        this.selectSearchDiv = createRef();
        this.selectIconChevron = createRef();
    }

    /**
     * Функция обработки события нажатия клавиш Enter и Esc в input'е 'select-search'
     * @param {React.KeyboardEvent<HTMLInputElement>} event - Событие нажатия клавиши на клавиатуре
     */
    KeyUp = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.code === "Enter") {
            this.setState({isAppear: true});
        }

        if(event.code === "Escape"){
            this.setState({isAppear: false});
        }
    }

    /**
     * Функция, при вызове которой, выпадающий список либо появляется, либо скрывается
     */
    AppearMenu = () => {
        this.setState({isAppear: !this.state.isAppear});
    }

    /**
     * Функция очистки символов с input'а 'select-search'
     */
    Clear = () => {
        transformToArray(this.selectPopupMenu.current!.children).forEach((item) => {
            if(item!.id !== '-1') {
                item!.className = 'select-popup_menu-row';
            }
        });

        this.setState({result: '', selected_items: [], itemIsSearch: true});
    }

    /**
     * Функция обработки выбора элемента, который выбрал пользователь из выпадающего списка
     * @param {React.ReactElement} child - Элемент из массива элементов, которые были приняты от родителя
     */
    SelectItem = (child: any) => {
        //добавление в массив выбранных пунктов новый пункт
        const arr: Array<string> = this.state.selected_items;

        let cash_result = '';
        //Перенос в временную переменную все значения из массива выбранных значений
        if(this.state.isMultiple) {
            if(!checkIndexOf(arr, child.value)) {
                arr.push(child.value);
            }
            arr.forEach((item) => cash_result += (item + ', '));
        }else{
            arr.push(child.value);
            cash_result = arr[0];
        }

        this.setState({ result: cash_result});

        this.props.options.map((item) => {
            const change_item: HTMLElement | null = document.getElementById(item.key);
            //Меняем цвет фона у элемента, который выбрали, а так же у всех предыдущих выбранных в массиве
            if(item.value === child.value || checkIndexOf(arr, item.value)){
                change_item!.className = 'select-popup_menu-row select-popup_menu-row--selected';
            } else {
                //У всех остальных оставляем прежний фон
                change_item!.className = 'select-popup_menu-row';
            }
        });

        if(!this.state.isMultiple) {
            this.AppearMenu();
        }
    }

    /**
     * Функция обработки живого поиска в input'е 'select-search'
     */
    SearchItem = () => {
        let input_value: string = this.selectInput.current!.value;

        this.setState({ result: input_value });

        //При множественном выборе для поиска обрезается левая часть с перечисленными значениями
        if(this.state.isMultiple) {
            let index_cut: number = this.state.selected_items.reduce((total, item) => {return (total + (item.length + 2))}, 0);
            input_value = input_value.replace(input_value.slice(0, index_cut), '');
        }

        this.setState({isLoading: true});

        //Производим поиск на серверной или клиентской части через метод debounce
        const debouncedTest = this.debounce(this.searchInProcess, 1000);
        debouncedTest(input_value);

    }

    /**
     * Функция отработки метода через заданный интервал времени
     * @param func - Функция, выполняющая логику
     * @param timeout - Время задержки
     */
    debounce<Params extends any[]>( func: (...args: Params) => any, timeout: number): (...args: Params) => void {
        let timer: NodeJS.Timeout;
        return (...args: Params) => {
            clearTimeout(timer)
            timer = setTimeout(() => { func(...args) }, timeout);
        };
    }

    /**
     * Функция поиска совпадения на сервере, либо на клиенте
     * @param input_value - Значение, которое ввел пользователь в input
     */
    searchInProcess = async (input_value: string) => {
        let count_searchItem: number = 0;
        await fetch(API.SEARCH_OPTIONS, requestOptions('POST', {value: input_value}))
            .then(response => response.json())
            .then(
                (data) => {
                    console.log('data', data);
                    //Допустим на сервере найденные совпадения записываются в массив items: string[]
                    //Если на сервере не было найдено совпадений
                    if (data.items === null) {
                        this.setState({itemIsSearch: false});
                    } else {
                        //Если есть совпадения
                        this.props.options.forEach((item) => {
                            count_searchItem = this.hiddenItem(item, checkIndexOf(data.items, item.value), input_value, count_searchItem);
                        });
                    }
                },
                (error) => {
                    //Если на сервере произошла ошибка, или не получилось соединиться, производиться поиск на клиентской части
                    console.log('error', error);
                    this.props.options.forEach((item) => {
                        count_searchItem = this.hiddenItem(item, (item.value.toLowerCase().split('').slice(0, input_value.length).join('') === input_value.toLowerCase()), input_value, count_searchItem);
                    });
                }
            );

        this.setState({itemIsSearch: count_searchItem > 0, isLoading: false});
    };

    /**
     * Функция скрытия элемента выпадающего списка
     * @param item - Элемент из массива элементов выпадающего списка
     * @param condition - Специальное условие
     * @param input_value - Значение с 'input-search'
     * @param count_searchItem - Количество элементов, которые совпали с значением с 'input-search'
     * @returns {number} Количество совпадений
     */
    hiddenItem = (item: any, condition: boolean, input_value: string, count_searchItem: number): number => {
        const change_item: HTMLElement | null = document.getElementById(item.key);
        //Изначально элементу ставится класс с свойством скрытия
        change_item!.className = 'select-popup_menu-row  select-popup_menu-row--hidden';

        //Если находим среди массива значений с сервера текущий элемент, то
        //меняется на класс с свойством отображения
        if (condition && input_value.length >= 0) {
            change_item!.className = 'select-popup_menu-row';
            count_searchItem++;
        }

        return count_searchItem;
    }

    /**
     * Создание списка опций, приходящих с родителя
     */
    componentDidMount(): void {
        this.props.options.forEach((item) => {
            let div = document.createElement("option");
            div.className = 'select-popup_menu-row';
            div.id = item.key;
            div.innerText = item.value;
            div.onclick = () => this.SelectItem(div);
            this.selectPopupMenu.current!.appendChild(div);
            console.log(div);
        });
    }

    /**
     * Функция отрисовки
     * @returns {JSX.Element} React-элемент
     */
    render(){
        return (
            <div className={'select_form'} onKeyUp={this.KeyUp}>
                <div className={'select-block'}>
                    <div className={'select-search' + (this.state.isAppear? ' select-search--active' : ' select-search--no_active' ) } id={'select-search'} ref={this.selectSearchDiv}>
                        <input type={"text"} id={'search_input'} name={'search_input'} placeholder={'Search...'} onClick={this.AppearMenu} value={this.state.result} onInput={this.SearchItem} autoComplete={'off'} ref={this.selectInput}/>
                        <div className={'select-icon'} onClick={this.Clear} id={'select-icon-delete'}>
                            <Close />
                        </div>
                        <div className={'select-icon' + (this.state.isAppear? ' icon_chevron_180' : ' icon_chevron_0')} onClick={this.AppearMenu} id={'select-icon-chevron'} ref={this.selectIconChevron}>
                            <Chevron />
                        </div>
                    </div>
                    <p className={this.state.isLoading? 'select-loader' : 'select-loader--hidden'}>Loading...</p>
                </div>

                <div id={'select-popup_menu'} className={'select-popup_menu' + (this.state.isAppear? ' select-popup_menu--appear' : ' select-popup_menu--disappear')} ref={this.selectPopupMenu}>
                <option id={'-1'} className={this.state.itemIsSearch? 'select-popup_menu-row select-popup_menu-row--hidden' : 'select-popup_menu-row'}>Ничего не найдено</option>
                </div>
            </div>
        );
    }
}

export default Select;