import ESLintPlugin from "eslint-webpack-plugin";
import {Configuration} from "webpack";

const config: Configuration = {

    plugins: [

    new ESLintPlugin({
        extensions: ["js", "jsx", "ts", "tsx", "css"],
    }),
],
};